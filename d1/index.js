let http=require("http");

//sample array database:

let courses= [
	{
		name: "English 101",
		price: 23000,
		isActive: true

	},
	{
		name: "Math 101",
		price: 25000,
		isActive: true

	},
	{
		name: "Reading 101",
		price: 21000,
		isActive: true

	}
];



http.createServer(function(req,res){
	//with an HTTP METHOD we cab actually routes that caters to the same endpoint but wwith a differenct actions.
	// MOst HTTP methods are primarily concered with CRU operations:

	//Common HTTP methods:
		//GET - Get method request indicates that the client wants to retrieve or GET data.
		//POST - post method request indicates that the liends wants to rcreate a resource
		//PUT - put method request indicates that the liends wants to update a resource
	   //DELETE - post method request indicates that the liends wants to delete a resource

console.log(req.url); //the request url endpoint
console.log(req.method); //the method of the request 

// SAMPLE FORMAT NO GET/POST/PUT/DELETE
// 	if(req.url==="/"){
// 		res.writeHead(200,{'Content-Type':'text/plain'});
// 		res.end("Welcome to our new server!");
// 	}else if(req.url==="/"){
// 		res.writeHead(200,{'Content-Type':'text/plain'});
// 		res.end("This is a new route!");
// 	}
// }).listen(4000);
// console.log("Server is running at localhost:4000");



	if(req.url==="/" && req.method==="GET"){
		//res.writeHead(200,{'Content-Type':'text/plain'});  //plain type display in text
		res.writeHead(200,{'Content-Type':'application/json'}); // change display type to json to display properly the array.



		//res.end("This is a response to a GET Method request!");
		res.end(JSON.stringify(courses));
		
		//we cannot pass another datatype other than string from our end()
		//so, to be able to pass array, we have to transfrom into JSON string.

	}else if(req.url==="/courses" && req.method==="POST"){
		// this route shouble able to receive frommthe client and we should be able to create a new course and add it to our courses array.

		//this will act as placeholder to contain the data passed from the client.

		let requestBody="";

		//recieving data from client to a nojejs server requires 2 stepsK:

			//data step - this part will read the stream of data coming from our client and process the incoming dta into the requestBody variable.

			req.on('data',function(data){
					console.log(data);
					requestBody +=data  //data stream is saved into the variable
			})

			// end step - this will run once or after the request data has been completely sent from our client.

		req.on('end',function(data){
					
					console.log(requestBody); //requestBody now contains data from our postman client
					// since requestBody is JSON format we have to parse to add it as an object in our array.

					requestBody = JSON.parse(requestBody);
		

		//simulate creating new document and adding 

		let newCourse ={
			//requested 
				name: requestBody.name,
				price: requestBody.price,
				isActive: true
			}
			console.log(newCourse)	;		
			courses.push(newCourse);
			//check if the new course was added into the courses array
			console.log(courses);		
		
		res.writeHead(200,{'Content-Type':'application/json'});
		res.end(JSON.stringify(courses));
	
		
	})

		// res.writeHead(200,{'Content-Type':'text/plain'});
		// res.end("This is a response to a POST Method request!");
	
	}else if(req.url==="/" && req.method==="PUT"){				//  you can change the root url with this format ->>>  "/courses"  
		res.writeHead(200,{'Content-Type':'text/plain'});		
		res.end("This is a response to a PUT Method request!");
	
	}else if(req.url==="/" && req.method==="DELETE"){
		res.writeHead(200,{'Content-Type':'text/plain'});
		res.end("This is a response to a DELETE Method request!");

	}
}).listen(4000);
console.log("Server is running at localhost:4000");


/*

mini activity





*/

//solution mini activity
/*
	if(req.url==="/course" && req.method==="GET"){
		res.writeHead(200,{'Content-Type':'text/plain'});
		res.end("This is a GET Method request for the /course end point!");
		res.end(`${re.method}`); //template literals
		

	}else if(req.url==="/course" && req.method==="POST"){
		res.writeHead(200,{'Content-Type':'text/plain'});
		res.end("This is a GET Method request for the /course end point!!");
	}
}).listen(4000);
console.log("Server is running at localhost:4000");

*/